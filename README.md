# ansible nvim plays

Used to setup a target client with nvim and needed plugins.


# Deployment

When using with vagrant for testing pull into ``provisioning`` directory. Make
sure to not create a new subdirectory containing this repo, but rather as if
the pull was provisioning directory itself.



```

cd <vagrant_env_for_nvim_tests>
git pull <nvim_ansible_play_repo> --directory provisioning
cd provisioning
mkdir files

```

files directory within provisioning is used for github ssh keys


Now should be able to deploy the VMs and start testing
